/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/

function Comment(name, text, avatarUrl) {
  this.name = name;
  this.text = text;
  if (avatarUrl) this.avatarUrl = avatarUrl;
  this.likes = 0;
}

Comment.prototype = {
  avatarUrl: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ8NDQ0NFREWFhURFRMYHSggGBolJxUVLT0tJikrLi4uFx8/ODMsOCguNSsBCgoKBwcHDgcHDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAwEBAQEAAAAAAAAAAAAAAwQFAgYBB//EADcQAQABAgIGBgkDBQEAAAAAAAABAgMEEQUVITFSkhNBYaLB0RIiMzRRcoKDsTKh4UJDcYGRI//EABUBAQEAAAAAAAAAAAAAAAAAAAAC/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/YQFJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGHjL1y7dm3TM5RVNNNMTlEzHXLcYWH97+7c8QNW3+zng1bf7OeGti8TTap9KdszspjrmWbGl68/0UZfDbn/ANBHq2/2c8Grb/Zzw2MNfpuUxVTu3THXE/BKDC1bf7OeDVt/s54boDC1bf7OeDVt/s54boDz1dN7D1UzMzTM7YyqzicuqW9ZuenRTVu9KmJy+GcMzTu+39fgv4H2Nv5KfwCcAAAAAAAAAAAAAAAAABhYf3v7tzxbrCw/vf3bniCfTlM/+c9XrR/vYynqblumuJpqiJid8SpRoq1nn68x8PS2A40JTMUVz1TVs/1G1pPlFMUxEREREbIiN0EzltndAEzlt3ZdbJxGlZ9OPQjOimduf9fki0jj+k9Sj9Eb54/4UAensXqblMVUznH7xPwlI83hMTVaqzjbE/qp6ph6CxepuUxVTOcfvE/CQZund9r6/BfwPsbfyU/hQ07vtfX4L+B9jb+Sn8AnAAAAAAAAAAAAAAAAAAYWH97+7c8W6wsP739254g3VDSOP6P1KNtc9z+XWksZNqIin9VWeU9VMfFhTOe2dsztmZBpaO0hNM+hcnOJ3VT1T29iPSOP6T1KNlEb54/4UAAABb0Zdqpu0xE7KpyqjqmFRYwHtrfzQC7p3fb+vwX8D7G38lP4UNO/2vr8F/A+xt/JT+ATgAAAAAAAAAAAAAAAAAMLD+9/dueLdYOKiuxfmvL+qa6ZndOfV+4LulcLcuTRNEZ5RMTtiOtR1Zf4Y5qUuuLnDb73ma4ucNvveYItWX+GOak1Zf4Y5qUuuLnDb73ma4ucNHe8wRasv8Mc1Jqy/wAMc1KXXFzht97zNcXOG33vMEWrL/DHNSlwmAu03KKqqYiIqzn1oNcXOG33vM1xc4bfe8wSad32/r8F/A+xt/JT+GLiMTXiKqY9GM4ziIpidubdw9v0KKKeummIn/OQJAAAAAAAAAAAAAAAAAAHyYidkxEx27X0Bx0VHBTywdFRwU8sOwHHRUcFPLB0VHBTyw7AcdFRwU8sHRUcFPLDsBx0VHBTywdFRwU8sOwHNNERuiI/xEQ6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH//2Q==',
  addLike: function(){this.likes++;}
}

let myComment1 = new Comment('первый', 'первый коментарий');
let myComment2 = new Comment('второй', 'второй коментарий', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOeoF-pnJHCw0KXpZEOVrqRX7IPO0vvyMJvg&usqp=CAU');
let myComment3 = new Comment('третий', 'third коментарий', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhMTExIWFhUXFxcbGBgYGRgaGhobGBodFxcaGhoYHSggGBolHRoYITEhJSkrLi4uGB8zODMsNygtLisBCgoKDg0OGxAQGzUmHyUtLS0tLS0tLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAIDBQYBBwj/xAA/EAABAwIEAwYFAgQFBAIDAAABAAIRAyEEEjFBBVFhBiJxgZGhEzKxwfBC0QdS4fEUFSMzYhZykqKCshckU//EABkBAAMBAQEAAAAAAAAAAAAAAAACAwEEBf/EACURAAICAgICAgIDAQAAAAAAAAABAhEDIRIxQVETMiJhcZGhI//aAAwDAQACEQMRAD8A9eo4JjYzOM+ymdRpEQYuslxntOynUa157pHdI59VR4jtMHVW5Xd29xpB0915KyxWoxPR+Cb3KRv63CKTrtcWnofsh3YFzbRI5/usXiu0TgJa64FwrzA9rWlryTGVrSfNY+EvFfwbxyR82HVMM4mP0oetQpgyYJHrdH4HiVN7cwdLuXKUDxSiAQ8nUieUHVRlBVaLQm7pjqGMZfIBO8arlDHOIMti91zAYGMzmNu46QrangDdxgGIjwRGMpBOcIlfgazX5u8ZBiEU+oI139wp28PY0uc7UxcW6LjsNRkam/NOoNIk8kW9WQfEkDwHvqhzXyjmWzIR1ajhw45hB2EwOqCq8PwxMB7pMj5iZ5zz2WSg/aNjOPp/0Ctxl3y4RyPVPdVa4gWJHJVPEOzpFN/wqgcYPzXJ2AEaFP4VwbEuDWulkmSTtFo1vMe6nxl0XuHdlriWNDYJuTAH0RPxGtph36iBPjquUuDsa/M+q50TrEEqwwdKnLzl1cfoFSON2QnljXsraXEe+Q6MrWgnxOg9CFHVx7HDNmsCdN4EkeiunU6U/INJ01hMyMIENAHQc4lO8b6sRZY3dGUwPFXVHtABl02OoDTeVM/jAplwP6Z/p5XV1isM0Fxa2Hc40BWRxnZ/IXvbWe9xmG7c4joYv0UuLidClGfgPp9oGOgh3dBv5/nsj8C+lWAeRMWn7Lzfgwe2jWbUEOzAgHWw0V9g+INFBrGyMt3R01vzWuNBprWjb1OKMpgGwBMBZXjPbGWd03Jj1MT4WWN472pPfp5iQ0SPEwf6LL4/ij4zdR5dfQT5qsYSn2TbhB67NNxXtXUa9uVx2/b6qy4V2zfIBcYgT4rzehNSq2dRBG8ARc9d1ZYyWNYYguM+A/TpudgneGK15M+aT2e58I7QCoQGxpMchoJPPdWeLaK9JwbeQV4r2b49ksST0+y9N7M8VAnO6J+q2DadMlOKauJkv+mHdV1ek5qfMLq6/ml7OX4o+jy/tFVp13ENe0VAZaHWBO7SeR0WX4tTy/JmpP8A5TOWdwDuCieOYBgc45srtbuj1/dZz/OatJ0OgjkDJHhJuuLDFtaPQytWEHjB7mckOAg8iNiiafF3STnlrh4XGyzWLfncX0zYn5dwTrZbDsV2ErYstNQmnT+YHnaPqryhFK2RU2y77GV6tQgNzEmZMWjZetUuEZwHVXZrCBFgm8H4XRw9JtNjRaLjcjdHvqTYGFz8Y3bCWSTVL+yX4wGghDPc4m266YbvdR1MTt9FspexIx9DzTeQQYA6eCipYZrZvdI1Tzt1UNWoR1+qm5Iooy6HYrhoqaut1G6o+IcFyszsqZnNdbMbTACNxlZ4FpmYHncKo4g2u9kXkXt0vcabJG0/BeEZLzozuJ7QVsK5zXDa45neNuXotH2c7VPxTGBstObKQQb9UO/gbsQC2ox/ebmDjfKQ4NgkeRHmtR2f7OMwoblu8zJOlztysqQha0LlyRXYTw/g5AeKr88vcWjSGnb63VtRoNaIAsk1nem2kJ69DFijFdHnTySl2zhYDsmsogaCE5zoCiqYprbON4mPDVO+C7FVvojq4UEyUFisCALDYid76/T3RwxrNzG8HXxThiWuHdM89lzyx430ysZzRk+JcCzD/baSXTNiQSIBWE7W4Z9Ci5jWFs/M46wZPlovWsbTqED4ZgzoeWg5rN8drNYHNxLQQ7faBsCdz9yuVx4M64Tc0fOb65OckmY++n0XavET8LINzJPWNF6F2j7IUXkvw7R39ASMu2g3Pt1WF7QdnK2FIDxIImRoN4Pku7HOE6OfJGcSDh+PDCZvIufsj62JLzJMkwANqbdfWIuqFtMgwbHrsiG18rcoOup5ppY1doyE3VMuMFiQ2p3TYaH7rc4TG2ZFvzf6rzDC1DmAC9Do4dzWtadYBPNc+WNFsbs1H+dHmks7mPIpKJSil7Tcfe1zqZIIG0Wjosg0guzA5TyvfwhaXtBgX1nzADtYn6LXdiexdOrkJF2lpdmF/oqwnHHD9mZIucn6Kn+HnZs4h5fUaMgEEXBcOROoI1FosvaMNhm06bKbQAG6RyRFHB0qTcrGtHgI0TqVMTdQm3JiqSodh6O6kqVQ3kFyrVEQDdVeJeAyXCFjfFUjIxc3bJsQ4mb2O6HpwBdwcPG/oqHG4xzo+G+AbaiZn2/Lo3hlIkHO4OvzPnM2jW8KXZ01xRcfEBAg+BlA4riFnX7w8tdLzawKkxVZjRENHgIgeV1T8Qp1A6WszsJEkG7QbEkEAm0HfVa0/BkUvIZieJwBnuYkAkSR5aqXB8YaRJFyPP8ArZZU4R79SHyTpIidMsaRp5BMqYKpSAqVK1OlSEw+s4NbOpAm7jbYHRPHG29GSlFLZ6FQ4k11OZ9CJ8vzdEcJ4kar3ZWwxo+Y6zsI9fZYOn2g4fTYKj8dTqOictLUzp82nmFXYD+LGGpfEFPDvIc8uc5zxvAmzV2YsGW030ceXJippdnrz6wAlCnijSSG3IsfGLgc4ssD/wDlfh1VuV9V7C7YMcQL7uA/JWn4dUw9eh8WjVbVZA7zXabkE6g6m/Mqk/kROCh5DsTxSm2Mzr8rQORPT85Km4hx9oqEsLTABIM33Fht1Kq8XgPiPEVe6e84npaPC2/PeFneI0qbZyv01AsCed9doXI5SZ1xxxRbYrtMS4h+bNqG5YHSwuT4wq6t2veH9wO7pu58ADo0DdUtTGgbBoOmknqQSUNWxQkAhp3FzB8JJHsjjY3JI2nD+1ea5cfEWbO1xcqzHEmPaC97ah0ykaE8h4aleW1eIAd4tJPIDbobFWXDuKZ75oGwv990jg0MpJl5xjHsa4ZW5TfUR8oEQLnqhxjm1GhpEzzEmefLwQtaux3efL+UjU+3og3HLcSzpv0jkVlDWZntb2ZLD8WncEnMLkhZENXpGIqlwLXgZeROsrMcQ4TlktiJXbhzaqRx5cSu0DdmcPmrN01XqXEeGmA/PfwJMbLzjBvNKHSJGnit9wDHGqwh7iS7VRztt2WwpVQDn6j0KStf8jf/AMfZJS5IpxNhiOy1KqcrhfnAkDoRBWg4dgG0KeRt7easqdPc6oPFO6rOHBWR+V5HRESZCmeWsuT5KDDYeLz3vqguM4poEPc0E6AgfXRJdKx+KlKjmLxLSTFibjlIWdxnEKkFrs3ledtrjzTsc0CAHuEjYD6zp1KzPE8Y+CM7OpLhJ9BJPmlUW2XtRWi8wWJaBAezOQZDssNjcl0ewKdX7RsptyU3MzmxygxPoB6rD4niBDCItyzZY5axKG4G/vk6N1JDjJ6K6x6Iuez0E0q7qeeqW5dS4ZmOvtcw7y3QmD4qM7qbahmWxHQF3uAbdVjO1/bIhraNEw4TnIAnl82v9kb/AAt7H4nHE4irUNOhNjEuqHfLyHVUjgbVk5ZknRqavHKWFa+s+SGsz1AJmS+GMHJziWj/AItkrxztT2lr46satZ1tGUx8lNuzWj6nUr1v+M3Zynh8BTGGou71UfFc2XOIa1xaX8wL32JC8MK7MOPgv2cuXJyYgVPWYWyFzCYcvMDQXcf5W7k/l07EVsz3GLEn30V10RfYOr3sn2lq4OrLXH4T7VWbObzjmNZ/dUZELgKVq9MZOto9pqdogxpJNi4ET/KBIv8A8nfZZt3HGVNSB1OvSPdb7svh8FxDhmHo4hgZWbTbTzxDgWiGnMIkEbG1ivGe13AK2AxL6NQmxljxIDm7Hx6LkjhV0dMszov8exnzgu8oM+JEoKhjmnVl97n9wqng/EnZsjjraUVW1Ijz39N0OFOmCle0WBDT3mCeYzAe5IEJ7K7mkEM8fz+6ApC/zZudxPoYhEMlps6OjrjygfVI0OmXtLGtInQ+3oq+riXTIcPCT91GxxI0b6H7FDuN4MeEf1ScUPyYQ/Fl0A2jYfunU3BwgX9VXOpZT463HpCLwmKEgAaaoa9AnYNj6cXI9NUbwfiDmxEjqVPiqQe0wFQ4Qim8h0+ZTfaIv1Ztf+oj0SWf/wAWz8P9UlLginNn0niqmyq8S6CN1Z14a3kqTEOa547wnr+WCTM3YmBEjq7wCdvf+qyPGeLtc4tzRz1nzARna3iNUANZAnQz+SVjiYJc55c6LyYDZ8f3UkrZ1JcVZPjsZqc74I0MgdNZKzuNffM+pI2AESeg390/GPfJc2Xco0n83KCEgZqjRmg21gdZ/t9+qEaOecr0QYirFyT5m3lzQ7+IVWU3ZOWoFxO87KWjTLnFzjmOm1ug6q2Zw6Gtkd55jXTqdzHJU0uye/Bney3Zupi6zMwIYXXJtmO4k+5+6+quHYRtGkyk0ANY0AAAAW5AaBeTdgcG5+NpBv8AtU2kzGsQZjqSPVewkrpxy5fkznyLjoixFVgBzkBsXzREb6r5v7cYDA0MTXbRawtDiRBN5NwBoACSLclqf4scXe5x+HUe3uw4SYIPhbr4LyjilX4kP5WO3pzVMeRMnkg+hnEMdmAY1rWt5NEDUm/OJQCdb9gmhM3YqVEjH7G6s8HUp0qjXZDrLZg26g2VTKLowR3jcaTuOS3lXYON9HrPCe0FN+RrqdNjC2CKcxINjGxnkudv8J/iMNJ776QkHdzDuF5/wzGAERAj8uvQOFY3PRDXXGhj+V1j6fZcmVvlyOnEvx4nkn+EqNMtBMaEK0ZVzR3b7jTztqPJWtTA/DqFjgO64gdQdD9lLjMAIzEeMWjyRKaYRhRXMb0B6X+h/dMNRgmZEdJC5VoGbX6f2KbUM6mD1O3ml7G6CsNXAEj9vz0UzHNd+o+GqrWVCw6gT6FNfOacx8AQR/RY4jJlxUY0ixdbkfrKHojrbloP6plK1405x912tU3AjmZSDFnRcQLCVTcRDw8uIjkNSjMLWvzHiFPjqrS24j0+6Fpgym/xB5D88kk3Mzmknr9GWfSXaHimWWiD0J/dUWCxDjOZjZ/mPtHVc49h2CpnJqT0cRfy1QbHMEk5oaJN7n/jM/1XnSblK2d0IqMKQBxSr3XNa3KCfmM5idxGw6BZek2akNa50bmzR1PVHcS4g+oS6IzHKxomw5nc+A5K24JwmnkLnEOcNWiXknl/K30VoqkRk7ZVOogAANBOri4kjz5DoLqrxVIwZ1Jjui/hJk+Q9VqCTeWtDT8re87TS03VTiqUGapLb92nEF3LSzR7qkWJIq+F4AB4ECwkzo3qfzXmrNuMYSXDvOPdaDo0W9zaRyhVtfE1HksY0Mbq4jSBp15aqXg+Blr3ExDsge7YuMEx+p+sNE3jkSmasVOj1jsBh/8AcqRBMeYOnhELVY58U3Hp+aLI/wAOca15rsaO6zJBMS75pJiw8B6lani73Ck7KATB10HU9F1Y9YjlybyHhXbw5qmaSDJgwQCNMwl3lJK85x5zd6SbkCSJ9rR4LY9suICpWqEumIHgAIHuSViMVVBJy6FGFG5WcZQnfntpyUbcubQx7p1AuuGk31TzhXA2v1CtZLi3tDsTRy3H51ldYyWgjUfkqOvUcLE7AeidhqwFneSzwC06DMILyPQW8wtv2dqyMpMg+1oMhYvCubz9CtX2eqQ6/mRoeqhkLQ7COPNBcwn5iyD1yy0/nVVOB4h+l+gNidvFWPamsAWDSQ6/mL+iz/xGm/yuEzMxax8gfYqaVoo3TCsSGTLe6eQ06IJ2Igw+/XX+iKdfVoI2vHodCPyFFWoVcsBgIPUGPI6pkKRGi13yzPWPooqdN4t+ymwTSOXgGwrGBHLxn6FY5GpAOWBEkqHEjMNfUJ+LpFpk5QDpFlFUccuknrqFiQwzBtLb6/nVWjngsIcBprb7rPMLpnTqrE4mG3v4ifeU0o7MTA7cz/6/sko87l1PQtntHF8XmrOBqCCbCSPcKPDPa6W1BYTlBBHjJ39t1DjMRTOZ2YOknnfnFoIQ+BqfFcym1wbF4aZMdYv0svMij0JNDMPh21Xk5clMGBqCRqTtAU9bHd8UsLhy/LpmJDZ5D8PnKsKpDalTIZJs551gbNGgvt+6gbiSxwawspN3ky4nckxJJOpNtr3VURZO+jiHAfFFRjyPkY4EADn8PSP+Sp8bXZSY4Sc51ebE7QCbAeCOqY4d4NdUeSP0ua0H7fQrJYv4jnxBA3yxMf8AcR+yeKtiy0iJnEWRkYHPMzlaIb0kk3vJ38eRdKm5+Uk2aSABa8XawfpHMhNpNYDlbla1olxuZcBp1I1PXlorDCs+RrR+l0QdXSQTJHiJ6Ep2TR6F/C6mAK0awyT1l3srXt/x2lhcM81DdzSAPEET4fuqX+F9ZuaowHMcgJOkw4iY5QvMv418e+NjX0QbUjkOt4h0/wDl9FfHuCRHJ92zL08HUxYr1WkZWmSJv3tPEyqqnhw2oGvsJg9BzW2/h1SDsLjBEnPSnwIJHu0rJdoxFYt0j73RCb+RwHljSxqbD8LwgPHcLS7cAtMeik/y9wgCmZdOUTExcrMhxBkEg891J/iX/wA7vUzfW6tUvf8AhDlH1/pf4DBCo5kCGvcRcXBGoQnaTDAYqpTbo3KP/UH7qtw+OqMIcyo4EaQTby0U+HxDqlbM92ZzzcnUk+CxqSd3oaMovVHcXgn0msfpmsCFedmuJgkNMA7+W49vRFdrcPlwVCRB+KPdjljadUtMgwkxv5IWzcq4To9F7S0pDJvAn1tKzTQWzo6QJ3uBBI8Y91e4rEyKczLWDXkYN/AgH1VZiaI05kQZ0m7TPpfxUoutFGgWhh2OmCWHWL78hvy5hGYa1jeffw3QFGpIyvs5vyvGvg7pqJRFItiHWPtPTb3WyBBlWo1uoy+/sNFE0AmQ4ekfdD4iraCC8eA+qVB3TL4Af2SNaGJ8S5paREnkq8vIbFh4gx7FHmheZ8UJjKBAvpz0nxhajCvYeYn1RtJgixI9I/dAh0Tl+y5hpvLiOgVWjLDvgDm388kkL8Ifzn2SS8f2HI9B4pR+MfnkCO43utEXO8lXXDCW0w2m00s9nOFzHjv6rOcHxLH1GG7iXalxEeh2816hh6LWTNMAADvzmGmpMmFxO1o67T2UzOEhlPQt7tiYk8r/AKSdbSfW2ZxlOnd7iMkyB8uaOc3I6/2Wv4rxBj6fw2Cckuc43Ava0XcV57xzFgd+pobNYOfPqep02TR29CPrYXjOIkiWsaxp0uJI5x8yz/Esf3cpBDdw2GjzMT7yuYzGU4u0udtsOsAET4lUjj8Rwc8ZWcx8x8JJKvCBKUjWMZTptbVqvhjQMrBbrAGpJtfz5BSYTGvI+IRBfIptP6WNsSek7dFU08PnqMNSzWtBAcZcBsYN5MWR5rtFMxY5YFpIbfSd4HusaBG07AYlzK7QKkNnKZF3bx4ak/crzH+JtLLxTFx+qpm9QJ95Wy7OV/h4qgxkW+Ym93R6u19lm/4o4X/98uH6mifEf3T4ZVKhcsLVoJ/hPX/1sVQ//rQzN6upOmP/ABe70WV7ViMZiByfH/i0D7JlPE1cO9lWk8se2crm6jMCDra4JHmgK9Zz3Oe8lznElxOpJuSVaEPzc/1RPJP8OH7sYkkkrHOJPw78rmu5OB9DKYkgDefxEJZRw1ImZfUf5ABo/wDsfRYnDU5qMHNw+qkFZ9Vwzvc/KIBc4ugawJ0CfSgVGeIUYR+OPE6Jv5HyNZxwQ9rrgZfTxG4sFWuqm7XfIQIPIiwvy380dxt85XToQPEGFTsq90N5WIPpE9R9FGK0PLs7VBDsxFjZ299J8LAjxUdTF5bC4PtH5psmMe5liJadDuJTMS3cxtzuqJezB/xpuGN8oHspqdWPmyg8iTKEYWg2g+Zn6IgEDkJ1G/jPNLJGoPa8gAxbTn+BNxzCWSJI5Sgmmox0AiD6T9ii8W9+S0TH90lU0b4KvCnW0+f7qHEVL6RGyVC/6gD1RFcBzcxAJG4Kv0xO1oBzt6+qS7lH8ySpons2XAB8N7YnuwTHTST9oK3DKtStmfUd8JjTroD0uLuPJeVcKxrmNLy+JMBbvh9SvWaO84t8osNra8ztK8/LCns7MUrWifiuMytd8M93f/47k7k+eoWQfVDpe85nkgNF4A3PUzbyVh2hxPwpa5xc4kQ39LesDUxzVG5xhoa2Cf7kn1W446sJvY3G1GZsol14PU/ZS4KqMxe+A1thyH/bzKHrYXJJm5IFtb6gJEBsSIA257SfOytqtEi2ZjQ/MQyJNnOOp0mNzG56ldxuKu0N3ETyA3HW/qeiFwzcxaSe6NvoBsAfVF1++O62GtiDpfUEk+vok1ZpbdjznxIJFmnW9ybeFgD7If8AikJr03bWHmf7LR9leE5WNce6DOs3m0+Ec/usr/EvGirU7kZWxFxeLfnilg/+iGeomX4gP9If9wHsVVq4qjPQnSIPiQFTrsxqlRy5tyskZSkE8tkwiwP5KKwh7ruhHvZMFL5h1EfZOhGlVg6Se9sG6YVoobwumSXcov5pjP8AcaOqJ4Owd658Pog2TnPiptbZdaikaniJzU4O4EHylp9RHmqMkggH9TYPlf1srvDAVKeQ906gxaVT1QSBcZg4j9lGHopI7RrTazm8hH0Khc/UBxjrB+6ir0CDIBE3HT8K4MQdCL79ZVK9CX7Gui9wD4LuHBJuV3ECfAJ+Aac31+y1/UF2WDWm0/35eKWOrDLlMeKdSZAiPBV+JIMglRirZR9ALmwdk/4hFv7JopXAK49hBgrp7OfaCJ/4JJkdSupCp2v3Yjn9FpeC8Vy0zmcb2jSeQ8FmMe7vlPwdSN7pZQ5RBTqRoeI0S8ZwJERPPwHkonSwZ3iALN+p8VBwvjLmyx08h0jWJ0ROK/1YzCLWHJvXqVFxcXT6KJp7K7H4kuyvJ1Ob0/dR8QxQzw28R7CP3PmhcUwzGsArpo5RmG491dJKiTbOuxri6SbTMBa7s18Su4OcIpg8uWp6rGYSiXOA+v8AVbN3FxSp/DaYcYAtNoubaSSf6pMq8IbH7ZqsXiHvdZ0bSdA3oBInr9VkO0+Etz6wfafsmYLiJcb5i7YE2J8BrHXRG8TwFR9MuqGeUEnxuRZc1OMlZ0WpIoOD087Hsae/lJEk/pEyPKVVnB5SM7pm4LZM3vchW3ZZ4FbKRz5TEbT+XR/EsLnZTe0ElhcTLnObAP6bwPIRbdd71s5a5L+DN0zkc5oMti8+qWKLcwLRzlFVMI4kuEQ65HRRvoWYXNjveqrpEqbB8RR0cf1aAdNT4KKrh3C8KzosaT1E5W7Cdb7lN/y97gHOMMzDmSeZA3CyUkgULJ6tN1Ok4d8ZQIDi2ROvy2CC4dgjUmHQ5FcUqD4cNaGhzrAToP8Auun8PlrflPp+QoTdR0WSuX8EVCpUo1Gyb6dD0un4+k19R2UwTeNPbYojEBtQfNfr+aoDEjNvDgNNCY0PVJF2bIa+ocsG6YYNxIOn9Cm4ereHCZ80q7S3TQp63Rg3EHS191PgXjf1QsHUIhlUAcuiJdUau7JcRjIken9VVveSZKdiKmYyo08IpIhOVs6HKdhkc4Q6kpVC241TNBGRLPiku/413JvoEklMpzOY2n3iRpsh2kg2V3xKi0OIJsJ9VWhokT+SshO0E8eyBtSDKuKPEhBLtbDx6AbAKtxNLSNFFVZlK1qMjLlEtGtBuDchCYomzdhp6rtPHQAI8TzKIqZS0OPXz5KdOL2UtSWgXCMOdvOfzzVsKZmCQ0nSRJ+qbwvWdhYR4dPyykxLBmLi7/1CWcrZsYh3C8M1jj/qidxkbYeQN+kq74jVLmAA3Is3S3MxosrSrNt3pvO4nw5Fahtdr6W0xBAiT47nzUMi3ZbG/Bj8G3LimCx73evbeRfVamq8HuB5+I4n/TGQNAM72mOZKpMFgHHE56bZNNrqhtIAFpO0X3K9GZ2ZwjMFQxpY99X4XxKwu4vMbNOmUjRsWnVdfNKCsgvs0ebPZllsR3TH1I8QoKwLz0B1npMKxxhbVbnYIGaW6yQfFUlao/5RaDr7DVVT8mSXgMwtKxDdYkdJ5o573BgAzENESCBM62PUc1zgmAc5ryTE6GNvyVqeyPZZlWrLz/oskvM6jQMBF2lxtOsSlnJJbGijB8Sb3qTSdibnr0RVVoa2xIgb6FWHaXCUhjarKTctOm/KBJMDK1xuepcEHiiAOfLdQySUmqNiq7Kv4xBBAB58/dcxeJDssQfrPIrlKc0jToPsuGmM9vafvon0K7GuokEEDkU+qYAtpz+iLLsti33Vdi8T+kW9/wCyyNyYOkhzcS0beiDq1JKYkrKKRCU2xJJLrQmFSEApWtEKMhFYOgSUknSKQWyOTyXVa/4boF1S+QrxIOK12uc6De8eGsoOr+nrCJ4hw5zHEKvdIMlNCq0ZO09hTn3J12QjmkyUc2ht0lMoMHf6BCkl0Eo32AIrB1JME9B08OqgITWmCqtWiK/Fmkp4ptIZWXM/VdqvcLuaFT4bEEvbHL38kZhHlz3G1iYG07LmlCjojKx1aveXa7D+srR9n6wqDvNgzuLR0PpuqN1EWJaD+coVtwvGOa8BmYDlsfISVOVNDp0y/wCGUQfjUwwyajHZYaczQw5bOIJOZ0ToJnWFd8T489h+FUaGvZGWCMrqbmggsOhbJcOkDXVZni1Eh9LFs2hr+gLhDhoc1yJ2zKs7SkVnul0PmQ4Cw2DZJ+SAPHXdWcPmxKHonGXxZXJ+TRcOwBxBrVKTAabQM1xqLuIB1CX/AE+GwHMdM2lh/VMC2pssK3idek3LEsiJYSWmOcdeaLZ2+xY1rP1BN9wAAehgKkHkgqasHKLdm7w3Baoblax5lpds2RYb6/1Q9LjDaVAnOGNB/wBoWiOYGp6rCu7X13GM7zAcGjMbBxlw8CoHNc4ZqkAzZo1M7G6llxzzVy1Q+PNGHRYDE/Eq1HmQXFzr9YBhVWIrndnmTqisPVdD3Hadf/I/ZVRu4ku3TSiuTolBtxCMDBM+osU5lOXEiCoWiDGyVatlmNSlq3ofojxVfbcfRAJxKQCvFUiEm5MauwurjlotCXWhNJTqZugE1YXh8PKtMIBZV9DEiUU2oJXNO32dKrwWWZvJJBfHHVJJQxo31aNd78p291QY/gpDri06/VVvDcdkJdvBjzVuzj+cBpERPnKPjlB6N5xktlR8U53mNoHTZAOcbq1xGIaSfBDV8OCJCtCVdolONrQG11k1P+GYXGCFayVPVnWGLq24XWFwbCxn6n0VQCnF5BKSUb0NGVGprVAAABE9JMKVmPDJMWiNb+E7DpyVNhMdpm5R+eaLqd5oi5J8fzRcrjT2XTNTwXjTHgsqNApvsSZJ6ACwA9BZZztdwU03F1Nz3MJsCQY3Mx+XQgEPgukiNDYeLhqeg9UeOIEjI+S3mefloE0W4O0ZJKSozNLGPZZTs4q7drT4gH6hFY7DNdJgbn3QIwt11LLaOf4nZ08TfECGj/iAPooqJe94iS718zKIZgxN7Iz47abSGC/Pnsllk9DKD8nKrcrRTBnmd51PkqqsYdZTuxPzEG8oN7pMrIRd7CcklSCjWiPND1akklNlcKdRSFlJsSSSSYmKUkkkAJJJJACBUwrlQgJ7AsdDxsk+OV1cgciupdD7IMyeKsTG6jSTUT5MUqV9ck+SiSRSMUmginXG6iLt0xKUcUNzbHgrh1TQnoBOyZzVPh8U6MoNov8AtPJCvfopcKBBU2tbLJ7JvjEEnwRFGt9UK4RJPknUXSJSNJoeyya2cwUNVsQmCvGa6ExONM2SRg2wckiavUgT090DVrkhRZ1yVeMKISyWclJJJUJCSSSQAkkkkAJJJJACSSSQAlJSco0gVjNTphOYJKH4i4l4leaGpJJJyIkkkkAJJJJACXUkkGodURWC/dJJTn9S0fscrrrf9tJJL4Q3kkrfnoq96SSbGLl6OJJJKhASSSSAEkkkgBJJJIASSSSAEkkkgBJJJIASSSSAP//Z');
let myComment4 = new Comment('четвертый', 'третий коментарий', 'https://avatarko.ru/img/kartinka/12/devushka_siluet_babochka_11386.jpg');


let CommentsArray = [myComment1, myComment2, myComment3, myComment4];

let commentsContainer = document.getElementById('CommentsFeed');

function Avatar(CommentsArray){
  this.CommentsArray = CommentsArray;

  this.Render = function(){
    this.CommentsArray.forEach( comment => {
      let block = document.createElement('div');

      let title = document.createElement('h3');
      title.innerText = comment.name;
      block.appendChild(title);

      let description = document.createElement('p');
      description.innerText = comment.text;
      block.appendChild(description); 

      let avatar = document.createElement('img');
      avatar.src = comment.avatarUrl;
      block.appendChild(avatar);

      let likes = document.createElement('div');
      likes.className = 'like';
      likes.innerText = comment.likes;
      block.appendChild(likes);

      block.addEventListener('click', function() {
        comment.addLike();
        block.querySelector('.like').innerHTML = comment.likes;
        console.log(comment.likes)
      })

      commentsContainer.appendChild(block);      
    })
  }

  this.Render();
}

let avatars = new Avatar(CommentsArray);
